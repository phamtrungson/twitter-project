﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TwitterProject.Model;

namespace TwitterProject.Features
{
    public static class GeneralFeatures
    {
        public static List<double> FeatureGenerator(List<int> list)
        {
            return list.Select(x => (double)x).ToList();
        }

        public static List<double> FeatureSelector(TimeSeri t, int startIndex, int length) 
        {
            return t.Data.Skip(startIndex).Take(length).ToList();
        }
    }
}
