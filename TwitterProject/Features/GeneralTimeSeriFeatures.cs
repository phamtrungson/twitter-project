﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TwitterProject.Model;

namespace TwitterProject.Features
{
    public static class GeneralTimeSeriFeatures
    {
        public static List<double> FeatureGenerator(List<int> input)
        {
            var l = 10;
            var result = new List<double>();
            for (var i = 0; i < input.Count; i++)
            {
                if (i < l)
                {
                    result.Add(0);
                }
                else
                {
                    result.Add(AverageStepIncreaseRate(input, 1, i - l + 1, i));
                }
            }
            return result;
        }

        public static List<double> FeatureSelector(TimeSeri t, int startIndex, int length)
        {
            var result = new List<double>();
            result.Add(t.Data[startIndex + length - 1]);
            return result;
        }

        public static double AverageStepIncreaseRate(List<int> input, int step, int startIndex, int endIndex)
        {
            double sum = 0;
            double sumAll = 0;
            var count = 0;
            if (startIndex == 0) throw new Exception("start index AverageStepIncreaseRate must be > 0");
            for (var i = startIndex; i <= endIndex; i++)
            {
                count++;
                var div = input[i - step] == 0 ? 1 : input[i - step];

                sum += (input[i] - input[i - step]) / div;                
            }
            if (count == 0) throw new Exception("no AverageStepIncreaseRate execute");
            return count == 0 ? 0 : sum / (count - step);
        }
    }
}
