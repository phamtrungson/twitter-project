﻿using Microsoft.ML.Runtime.Api;
using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterProject.Model
{
    public class MySampleData
    {        
        [Column("0")]
        public float Attr;

        [Column("1")]  
        [ColumnName("Label")]
        public float Label;
    }

    public class MySamplePrediction
    {
        [ColumnName("PredictedLabel")]
        public float PredictedLabels;
    }
}
