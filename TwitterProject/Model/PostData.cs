﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterProject.Model
{
    public class PostData
    {
        public string Eventid { get; set; }
        public string Dstmid { get; set; }
        public string Dstuid { get; set; }
        public long Dsttime { get; set; }
        public DateTime ParsedDsttime { get; set; }

        public string Srcmid { get; set; }
        public string Srcuid { get; set; }
        public long Srctime { get; set; }
        public DateTime ParsedSrctime { get; set; }

        public string Rtmid { get; set; }
        public string Rtuid { get; set; }
        public long Rttime { get; set; }
        public DateTime ParsedRttime { get; set; }
    }

    public class PostDataRawMap : ClassMap<PostData>
    {
        public PostDataRawMap()
        {
            Map(m => m.Eventid).Name("event_id").Index(0);
            Map(m => m.Dstmid).Name("dstmid").Index(1);
            Map(m => m.Dstuid).Name("dstuid").Index(2);
            Map(m => m.Dsttime).Name("dsttime").Index(3);
            Map(m => m.Srcmid).Name("srcmid").Index(4);
            Map(m => m.Srcuid).Name("srcuid").Index(5);
            Map(m => m.Srctime).Name("srctime").Index(6);
            Map(m => m.Rtmid).Name("rtmid").Index(7);
            Map(m => m.Rtuid).Name("rtuid").Index(8);
            Map(m => m.Rttime).Name("rttime").Index(9);

            Map(m => m.ParsedDsttime).Ignore();
            Map(m => m.ParsedSrctime).Ignore();
            Map(m => m.ParsedRttime).Ignore();
        }
    }

    public class PostDataTransformedMap : ClassMap<PostData>
    {
        public PostDataTransformedMap()
        {
            Map(m => m.Eventid).Index(0);
            Map(m => m.Dstmid).Index(1);
            Map(m => m.Dstuid).Index(2);
            Map(m => m.Dsttime).Ignore();
            Map(m => m.ParsedDsttime).Index(3);

            Map(m => m.Srcmid).Index(4);
            Map(m => m.Srcuid).Index(5);
            Map(m => m.Srctime).Ignore();
            Map(m => m.ParsedSrctime).Index(6);

            Map(m => m.Rtmid).Ignore();
            Map(m => m.Rtuid).Ignore();
            Map(m => m.Rttime).Ignore();
            Map(m => m.ParsedRttime).Ignore();
        }
    }
}
