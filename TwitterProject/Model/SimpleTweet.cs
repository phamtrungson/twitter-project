﻿using System;
using System.Collections.Generic;
using System.Text;
using Tweetinvi.Models;

namespace TwitterProject.Model
{
    public class SimpleTweet
    {
        public long Id { get; set; }
        public int RetweetCount { get; set; }
        public int FavoriteCount { get; set; }

        public DateTime CreatedAt { get; set; }
        public string Text { get; set; }
        public string FullText { get; set; }
        public long CreatedBy { get; set; }
    }

    public class TopicSummary
    {
        public DateTime MinCreatedAt { get; set; }
        public DateTime MaxCreatedAt { get; set; }
        public int NumberOfTweets { get; set; }
        public int Id { get; set; }
        public List<SimpleTweet> Tweets { get; set; }
    }

    public class Event
    {
        public int EventId { get; set; }
        public List<ITweet> Tweets { get; set; }
    }
}
