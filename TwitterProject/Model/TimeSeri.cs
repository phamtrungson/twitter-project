﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TwitterProject.Model
{
    public class TimeSeri
    {
        public string Id { get; set; }
        public List<int> ObjCountList { get; set; }
        public List<double> Data { get; set; }
        public int MaxIndex { get; set; }
        public int MaxValue { get; set; }
        public double MaxValuePercent { get; set; }
    }

    public class TimeSeriPattern
    {
        public string Id { get; set; }
        public List<double> Features { get; set; }
        public int Label { get; set; }
    }
}
