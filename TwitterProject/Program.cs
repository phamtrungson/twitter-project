﻿using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Runtime.Api;
using Microsoft.ML.Trainers;
using Microsoft.ML.Transforms;
using System;
using System.Collections.Generic;
using System.Linq;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;
using TwitterProject.Features;
using TwitterProject.Model;
using TwitterProject.Services;

namespace TwitterProject
{
    class Program
    {

        static void Main(string[] args)
        {
            var rawDataService = new DataService();
            var topics = rawDataService.ReadCrawledTopic();
            var userCredentials = Auth.CreateCredentials("0FKTt1fsBuwpU1BM4OjoRZIK3", "LmCYHkt5WFgSFBeMtsnByfVE7vw8eHEfveL2YVyAshCu6WS6Fl",
                "3308729503-vDh1CCxFDAflKKmuo65OmGTnNOuMXXOhfUtJUzz",
                "KozxAbQgszLPumrD7gl7EfcOKp2IM6lSyjfSizI12q44f");
            Auth.SetCredentials(userCredentials);

            int i = 0;
            foreach (var topic in topics)
            {
                Console.WriteLine($"Process event #{i}, event id = {topic.Id}");
                var tweets = Tweet.GetTweets(topic.Tweets.Select(x => x.Id).ToArray());
                rawDataService.SaveTopicFull(topic.Id, tweets);
                Console.WriteLine($"Finish event #{i} =======================");
            }
            Console.ReadLine();
        }

        static void OldImplement()
        {
            //var service = new DataTransformService();
            //service.CreateParttern();

            var rawDataService = new DataService();
            var data = rawDataService.ReadCrawledTopic();

            var service = new Event2012Service();
            var dataService = new Event2012DataService();

            var hash = Guid.NewGuid();
            // filter
            var filteredData = service.FilterTweets(data);

            // map
            var timeSeri = service.MapToTimeSeries(filteredData, x => x.Tweets, x => x.CreatedAt, x => x.MaxCreatedAt, x => x.MinCreatedAt,
                x => x.Id.ToString());

            var filteredTimeSeri = service.FilterTimeSeri(timeSeri);

            service.GenerateFeature(filteredTimeSeri, GeneralTimeSeriFeatures.FeatureGenerator);

            // generate Pattern
            var patterns = service.GeneratePattern(filteredTimeSeri, GeneralTimeSeriFeatures.FeatureSelector);
            dataService.SaveTimeSeriPatternCsv(patterns, "output/event2012-pattern", hash);

            Console.ReadLine();
        }
    }    
}
