﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using Tweetinvi.Models;
using Newtonsoft.Json;
using TwitterProject.Model;
using System.Text.RegularExpressions;

namespace TwitterProject.Services
{
    public class DataService
    {
        public List<long> ReadTopic(int topicId)
        {
            var result = new List<long>();
            using (var streamReader = new StreamReader(@"C:\E\1.Study\Master4\De cuong\Events2012\Events2012\relevant_tweets.tsv"))
            {
                while (true && !streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    if (String.IsNullOrWhiteSpace(line)) { continue; }

                    var data = line.Split(new char[] { ' ', '\t' });
                    (int TopicId, long TweetId) lineData = (int.Parse(data[0]), long.Parse(data[1]));
                    if (lineData.TopicId != topicId) { continue; }
                    result.Add(lineData.TweetId);
                }
            }
            return result;
        }

        public List<long> ReadTopics()
        {
            var result = new List<long>();
            using (var streamReader = new StreamReader(@"C:\E\1.Study\Master4\De cuong\Events2012\Events2012\relevant_tweets.tsv"))
            {
                while (true && !streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    if (String.IsNullOrWhiteSpace(line)) { continue; }

                    var data = line.Split(new char[] { ' ', '\t' });
                    (int TopicId, long TweetId) lineData = (int.Parse(data[0]), long.Parse(data[1]));
                    result.Add(lineData.TweetId);
                }
            }
            return result;
        }

        public List<string> ReadDescription()
        {
            var result = new List<string>();
            using (var streamReader = new StreamReader(@"C:\E\1.Study\Master4\De cuong\Events2012\Events2012\event_descriptions.tsv"))
            {
                while (true && !streamReader.EndOfStream)
                {
                    var line = streamReader.ReadLine();
                    if (String.IsNullOrWhiteSpace(line)) { continue; }

                    var data = line.Split(new char[] { ' ', '\t' }, 2);
                    (int TopicId, string des) lineData = (int.Parse(data[0]), data[1].Substring(1, data[1].Length - 2));
                    result.Add(lineData.des);
                }
            }
            return result;
        }

        public bool SaveTopic(int topicId, List<ITweet> tweets)
        {
            using (var streamWriter = File.CreateText($"{topicId}.txt"))
            {
                var mapped = tweets.Select(x => new SimpleTweet
                {
                    Id = x.Id,
                    CreatedBy = x.CreatedBy.Id,
                    CreatedAt = x.CreatedAt,
                    FullText = x.FullText,
                    Text = x.Text,
                    RetweetCount = x.RetweetCount,
                    FavoriteCount = x.FavoriteCount,

                });

                foreach (var x in mapped)
                {
                    streamWriter.WriteLine(JsonConvert.SerializeObject(x));
                };
            }
            return true;
        }

        public List<TopicSummary> ReadCrawledTopic()
        {
            var results = new List<TopicSummary>();
            var dir = new DirectoryInfo(@"Crawl/");
            var files = dir.GetFiles();
            foreach (var file in files)
            {
                var id = int.Parse(new Regex(@"^Topic-(\d*)-Tweets(.*)$").Matches(file.Name)[0].Groups[1].Value);
                var listResult = new List<SimpleTweet>();
                using (var streamReader = new StreamReader(file.FullName))
                {
                    while (!streamReader.EndOfStream)
                    {
                        var line = streamReader.ReadLine();
                        var obj = JsonConvert.DeserializeObject<SimpleTweet>(line);
                        listResult.Add(obj);
                    }
                }
                if (listResult.Count == 0) continue;

                results.Add(new TopicSummary
                {
                    MaxCreatedAt = listResult.Max(x => x.CreatedAt),
                    MinCreatedAt = listResult.Min(x => x.CreatedAt),
                    NumberOfTweets = listResult.Count,
                    Id = id,
                    Tweets = listResult
                });
            }
            return results;
        }

        public bool SaveTopicFull(int topicId, IEnumerable<ITweet> tweets)
        {
            using (var streamWriter = File.CreateText($"Output/{topicId}.txt"))
            {
                streamWriter.WriteLine(JsonConvert.SerializeObject(tweets));
            }
            return true;
        }
    }
}
