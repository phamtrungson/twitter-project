﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TwitterProject.Model;

namespace TwitterProject.Services
{
    public class DataTransformService
    {
        public void TransformFromRaw()
        {
            List<PostData> records;
            using (var csv = new CsvReader(new StreamReader("dataset/micropost_route_info.csv")))
            {
                csv.Configuration.RegisterClassMap(new PostDataRawMap());
                records = csv.GetRecords<PostData>().ToList();

            }
            records.ForEach(x =>
            {
                x.ParsedSrctime = ParseTimeFromMiliseconds(x.Srctime);
                x.ParsedDsttime = ParseTimeFromMiliseconds(x.Dsttime);
                x.ParsedRttime = ParseTimeFromMiliseconds(x.Rttime);
            });

            var groups = records.GroupBy(x => x.Eventid).ToDictionary(x => x.Key, x => x.ToList());

            var stats = groups.Select(x =>
            {
                return new
                {
                    EventId = x.Key,
                    ParsedSrctimeMax = x.Value.Max(v => v.ParsedSrctime),
                    ParsedSrctimeMin = x.Value.Min(v => v.ParsedSrctime),
                    ParsedRttimeMax = x.Value.Max(v => v.ParsedRttime),
                    ParsedRttimeMin = x.Value.Min(v => v.ParsedRttime),
                    ParsedDsttimeMax = x.Value.Max(v => v.ParsedDsttime),
                    ParsedDsttimeMin = x.Value.Min(v => v.ParsedDsttime)
                };
            }).ToList();

            var allStats = new
            {
                ParsedSrctimeMax = stats.Max(v => v.ParsedSrctimeMax),
                ParsedSrctimeMin = stats.Min(v => v.ParsedSrctimeMin),
                ParsedRttimeMax = stats.Max(v => v.ParsedRttimeMax),
                ParsedRttimeMin = stats.Min(v => v.ParsedRttimeMin),
                ParsedDsttimeMax = stats.Max(v => v.ParsedDsttimeMax),
                ParsedDsttimeMin = stats.Min(v => v.ParsedDsttimeMin)
            };


            using (var csvWrite = new CsvWriter(File.CreateText("output/transformed.csv")))
            {
                csvWrite.Configuration.RegisterClassMap(new PostDataTransformedMap());
                csvWrite.WriteRecords(records);
            }

            using (var csvWrite = new CsvWriter(File.CreateText("output/stats.csv")))
            {
                csvWrite.WriteRecords(stats);
            }

            using (var csvWrite = new CsvWriter(File.CreateText("output/allStats.csv")))
            {
                csvWrite.WriteRecords(new List<object> { allStats });
            }

        }

        public void TransformToTimeSeries()
        {
            List<PostData> records;
            using (var csv = new CsvReader(new StreamReader("dataset/micropost_route_info.csv")))
            {
                csv.Configuration.RegisterClassMap(new PostDataRawMap());
                records = csv.GetRecords<PostData>().ToList();

            }

            var groups = records.GroupBy(x => x.Eventid).ToList();

            var NumberSlot = 100;
            var result = new List<(string EventId, List<int> TimeSeri, int MaxIndex)>();

            groups.ForEach(x =>
            {
                var list = x.ToList();
                var minDst = list.Min(t => t.Dsttime);
                var maxDst = list.Max(t => t.Dsttime);
                var range = maxDst - minDst;

                var resutList = Enumerable.Repeat(0, NumberSlot).ToList();
                list.ForEach(t => {
                    resutList[(int)Math.Round((t.Dsttime - minDst) / ((double)range) * (NumberSlot - 1))]++;
                });

                var maxIndex = resutList.IndexOf(resutList.Max());

                result.Add((x.Key, resutList, maxIndex));
            });

            var csvResult = result.Select(x =>
            {
                var dicToBuild = new Dictionary<string, object>();
                dicToBuild.Add("EventId", x.EventId);
                var i = 0;
                x.TimeSeri.ForEach(t =>
                {                    
                    dicToBuild.Add(i.ToString(), t);
                    i++;
                });
                dicToBuild.Add("Label", x.MaxIndex);
                return dicToBuild.BuildCsvObject();
            });

            using (var csvWrite = new CsvWriter(File.CreateText($"output/count_timeseri_{NumberSlot}.csv")))
            {                
                csvWrite.WriteRecords(csvResult);
            }
        }



        public void CreateParttern()
        {
            var PatternLength = 20;
            var HalfPatternLength = 10;
            var OneHalfPatternLength = 30;

            var result = ExtractCount();
            var selectedPositive = result
                .Where(x => x.MaxIndex >= PatternLength - 1)
                .Select(x => {
                    return (x.EventId, TimeSeri: x.TimeSeri.Skip(x.MaxIndex - PatternLength - 1).Take(PatternLength).ToList(), Label: true);
                }).ToList();

            var selectedNegative = new List<(string EventId, List<int> TimeSeri, bool Label)>();
            var random = new Random();
            result.ForEach(x =>
            {
                var choosenStart = 0;
                if (x.MaxIndex < HalfPatternLength - 1)
                {
                    choosenStart = 0;
                }
                else if (x.MaxIndex < PatternLength - 1)
                {
                    choosenStart = Seed2();
                }
                else if (x.MaxIndex >= x.TimeSeri.Count - HalfPatternLength - 1)
                {
                    choosenStart = Seed1();
                }
                else
                {
                    choosenStart = random.Next(1) == 1 ? Seed1() : Seed2();
                }
                selectedNegative.Add((x.EventId, TimeSeri: x.TimeSeri.Skip(choosenStart).Take(PatternLength).ToList(), Label: false));
                int Seed1()
                {
                    var indexBeforePositive = x.MaxIndex + 1 - OneHalfPatternLength;
                    return random.Next(0, indexBeforePositive);
                }
                int Seed2()
                {
                    var indexAfterPositive = x.MaxIndex + 1 - HalfPatternLength;
                    return random.Next(indexAfterPositive, x.TimeSeri.Count - PatternLength - 1);
                }
            });

            Save("positive", selectedPositive);
            Save("negative", selectedNegative);
        }

        public DateTime ParseTimeFromMiliseconds(long seconds)
        {
            TimeSpan time = TimeSpan.FromSeconds(seconds);
            return new DateTime(1970, 1, 1) + time;
        }

        public void Save(string fileName, List<(string EventId, List<int> TimeSeri, bool Label)> data)
        {
            var csvResult = data.Select(x =>
            {
                var dicToBuild = new Dictionary<string, object>();
                dicToBuild.Add("EventId", x.EventId);
                var i = 0;
                x.TimeSeri.ForEach(t =>
                {
                    dicToBuild.Add(i.ToString(), t);
                    i++;
                });
                dicToBuild.Add("Label", x.Label);
                return dicToBuild.BuildCsvObject();
            });

            using (var csvWrite = new CsvWriter(File.CreateText($"output/{fileName}.csv")))
            {
                csvWrite.WriteRecords(csvResult);
            }
        }

        public List<(string EventId, List<int> TimeSeri, int MaxIndex)> ExtractCount()
        {
            List<PostData> records;
            using (var csv = new CsvReader(new StreamReader("dataset/micropost_route_info.csv")))
            {
                csv.Configuration.RegisterClassMap(new PostDataRawMap());
                records = csv.GetRecords<PostData>().ToList();

            }

            var groups = records.GroupBy(x => x.Eventid).ToList();

            var NumberSlot = 100;
            var result = new List<(string EventId, List<int> TimeSeri, int MaxIndex)>();

            groups.ForEach(x =>
            {
                var list = x.ToList();
                var minDst = list.Min(t => t.Dsttime);
                var maxDst = list.Max(t => t.Dsttime);
                var range = maxDst - minDst;

                var resutList = Enumerable.Repeat(0, NumberSlot).ToList();
                list.ForEach(t => {
                    resutList[(int)Math.Round((t.Dsttime - minDst) / ((double)range) * (NumberSlot - 1))]++;
                });

                var maxIndex = resutList.IndexOf(resutList.Max());

                result.Add((x.Key, resutList, maxIndex));
            });
            return result;
        }
    }
}
