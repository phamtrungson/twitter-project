﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TwitterProject.Model;

namespace TwitterProject.Services
{
    public class Event2012DataService
    {
        public void SaveTimeSeriCsv(List<TimeSeri> result, string fileName = "output/timeseri", Guid? guid = null)
        {
            var csvResult = result.Select(x =>
            {
                var dicToBuild = new Dictionary<string, object>();
                dicToBuild.Add("Id", x.Id);
                var i = 0;
                x.Data.ForEach(t =>
                {
                    dicToBuild.Add(i.ToString(), t);
                    i++;
                });
                dicToBuild.Add("MaxIndex", x.MaxIndex);
                dicToBuild.Add("MaxValue", x.MaxValue);
                dicToBuild.Add("MaxValuePercent", x.MaxValuePercent);
                return dicToBuild.BuildCsvObject();
            });

            using (var csvWrite = new CsvWriter(File.CreateText($"{fileName}-{(guid ?? Guid.NewGuid())}.csv")))
            {
                csvWrite.WriteRecords(csvResult);
            }
        }

        public void SaveTimeSeriObjCountCsv(List<TimeSeri> result, string fileName = "output/timeseri-objcount", Guid? guid = null)
        {
            var csvResult = result.Select(x =>
            {
                var dicToBuild = new Dictionary<string, object>();
                dicToBuild.Add("Id", x.Id);
                var i = 0;
                x.ObjCountList.ForEach(t =>
                {
                    dicToBuild.Add(i.ToString(), t);
                    i++;
                });
                dicToBuild.Add("MaxIndex", x.MaxIndex);
                dicToBuild.Add("MaxValue", x.MaxValue);
                dicToBuild.Add("MaxValuePercent", x.MaxValuePercent);
                return dicToBuild.BuildCsvObject();
            });

            using (var csvWrite = new CsvWriter(File.CreateText($"{fileName}-{(guid ?? Guid.NewGuid())}.csv")))
            {
                csvWrite.WriteRecords(csvResult);
            }
        }

        public void SaveTimeSeriPatternCsv(List<TimeSeriPattern> result, string fileName = "output/pattern", Guid? guid = null)
        {
            var csvResult = result.Select(x =>
            {
                var dicToBuild = new Dictionary<string, object>();
                dicToBuild.Add("Id", x.Id);
                var i = 0;
                x.Features.ForEach(t =>
                {
                    dicToBuild.Add(i.ToString(), t);
                    i++;
                });
                dicToBuild.Add("Label", x.Label);
                return dicToBuild.BuildCsvObject();
            });

            using (var csvWrite = new CsvWriter(File.CreateText($"{fileName}-{(guid ?? Guid.NewGuid())}.csv")))
            {
                csvWrite.WriteRecords(csvResult);
            }
        }
    }
}
