﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TwitterProject.Features;
using TwitterProject.Model;

namespace TwitterProject.Services
{
    class Event2012Service
    {
        public List<TopicSummary> FilterTweets(List<TopicSummary> input)
        {
            // results.Where(x => x.Item3.NumberOfTweets > 20 && x.Item3.MaxCreatedAt - x.Item3.MinCreatedAt > new TimeSpan(0, 1, 0, 0));
            return input.Where(x => x.NumberOfTweets >= 50).ToList();
            // return input.Where(x => x.NumberOfTweets >= 50 && x.MaxCreatedAt - x.MinCreatedAt >= new TimeSpan(1, 0, 0, 0)).ToList();
        }

        public List<TimeSeri> FilterTimeSeri(List<TimeSeri> input)
        {
            // results.Where(x => x.Item3.NumberOfTweets > 20 && x.Item3.MaxCreatedAt - x.Item3.MinCreatedAt > new TimeSpan(0, 1, 0, 0));
            return input.Where(x => x.MaxValuePercent >= 0.1).ToList();
        }

        public List<TimeSeri> MapToTimeSeries<T, TEntity>(List<T> inputs, Func<T, List<TEntity>> listSelector, Func<TEntity, DateTime> createdAtSelector, Func<T, DateTime> maxSelector, Func<T, DateTime> minSelector,
            Func<T, string> idSelector,
            int numSlot = 100) 
            where T : class where TEntity: class
        {
            var result = new List<TimeSeri>();
            inputs.ForEach(x =>
            {
                var list = listSelector(x);
                var max = maxSelector(x);
                var min = minSelector(x);
                
                var range = max - min;

                var resultList = Enumerable.Repeat(0, numSlot).ToList();
                list.ForEach(t => {
                    var createdAt = createdAtSelector(t);
                    if (range == TimeSpan.Zero)
                    {
                        resultList[0]++;
                        return;
                    }
                    resultList[(int)Math.Round((createdAt - min) / ((TimeSpan)range) * (numSlot - 1))]++;
                });

                var maxValue = resultList.Max();
                var maxIndex = resultList.IndexOf(maxValue);

                

                result.Add(new TimeSeri
                {
                    Id = idSelector(x),
                    ObjCountList = resultList,                    
                    MaxIndex = maxIndex,
                    MaxValue = maxValue,
                    MaxValuePercent = maxValue / (double)resultList.Sum()
                });
            });
            return result;
        }

        public void GenerateFeature(List<TimeSeri> input, Func<List<int>, List<double>> featureGenerator = null)
        {
            featureGenerator = featureGenerator ?? GeneralFeatures.FeatureGenerator;
            input.ForEach(x => x.Data = featureGenerator(x.ObjCountList));
        }

        public List<TimeSeriPattern> GeneratePattern(List<TimeSeri> input, Func<TimeSeri, int,int, List<double>> featureSeletor = null)
        {
            var PatternLength = 20;
            var HalfPatternLength = 10;
            var OneHalfPatternLength = 30;

            featureSeletor = featureSeletor ?? GeneralFeatures.FeatureSelector;

            var selectedPositive = input
                .Where(x => x.MaxIndex >= PatternLength - 1)
                .Select(x => {
                    return new TimeSeriPattern { Id = x.Id, Features = featureSeletor(x, x.MaxIndex - (PatternLength - 1), PatternLength), Label = 1 };
                }).ToList();
           
            var random = new Random();
            var Nega = 3;
            for (var i = 0; i < Nega; i++)
            {
                var selectedNegative = new List<TimeSeriPattern>();
                input.ForEach(x =>
                {
                    var choosenStart = 0;
                    if (x.MaxIndex < HalfPatternLength - 1)
                    {
                        choosenStart = 0;
                    }
                    else if (x.MaxIndex < PatternLength - 1)
                    {
                        choosenStart = Seed2();
                    }
                    else if (x.MaxIndex >= x.ObjCountList.Count - HalfPatternLength - 1)
                    {
                        choosenStart = Seed1();
                    }
                    else
                    {
                        choosenStart = random.Next(1) == 1 ? Seed1() : Seed2();
                    }
                    selectedNegative.Add(new TimeSeriPattern { Id = x.Id, Features = featureSeletor(x, choosenStart, PatternLength), Label = 0 });
                    int Seed1()
                    {
                        var indexBeforePositive = x.MaxIndex + 1 - OneHalfPatternLength;
                        return random.Next(0, indexBeforePositive);
                    }
                    int Seed2()
                    {
                        var indexAfterPositive = x.MaxIndex + 1 - HalfPatternLength;
                        return random.Next(indexAfterPositive, x.ObjCountList.Count - PatternLength - 1);
                    }
                });
                selectedPositive.AddRange(selectedNegative);
            }
            return selectedPositive;
        }
    }
}
